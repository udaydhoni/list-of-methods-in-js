# Methods


## flat

What are the parameters?

It accepts the depth parameter.

What will it return?

A new array with sub array elements concatenated upto a specific depth.

Give an Example.

let arr=[1,2,[3,[4]]];

console.log(arr.flat(1)); // [1,2,3,[4]]

Write in your own words what "flat" means?

flat accesses the sub array elements upto a specified depth and adds them to a new array.

Does it mutate the original array?

No

## Concat

What are the parameters?

It accepts any number of parameters and those parameters can be of any type.

What will it return?

It returns a new array.

Give an Example.

let arr = [1,2,3]

console.log(arr.concat(4,5,true)); // [1,2,3,4,5,true];

Write in your own words what "concat" means?

It just merges two arrays or adds the values we have passed to the initial array and returns a new array;

Does it mutate the original array?

No

## push


What are the parameters?

Any number of parameters and it accepts any type of parameter.

What will it return?

It will return the original array length after adding the passed in parameters to it.

Give an Example.

let arr = [1,2,3]

console.log(arr.push(4)); // 4

console.log(arr); // [1,2,3,4]

Write in your own words what "push" means?

It adds values at the end of the array without disturbing the order in which they're passed.

Does it mutate the original array?

Yes it mutates.


## pop


What are the parameters?

It does not have any parameters.

What will it return?

It returns the last element which is popped out of the original array.

Give an Example.

let arr = [1,2,3];

console.log(arr.pop()); // 3

console.log(arr); // [1,2]

Write in your own words what "pop" means?

It pops out the last element from the given array.

Does it mutate the original array?

Yes it mutates the original array.

## shift

What are the parameters?

It does not have any parameters.

What will it return?

It returns the first element that has been removed from the array.

Give an Example.

let arr = [1,2,3,4]

console.log(arr.shift()); // 1

Write in your own words what "shift" means?

It removes the first element from the given array.

Does it mutate the original array?

It does mutate the original array.

## unshift

What are the parameters?

It accepts any number of parameters.

What will it return?

It will return the length of the updated array;

Give an Example.

let arr = [1,2,3];

arr.unshift(4,5,6)

console.log(arr.unshift(4,5,6)); // 6

console.log(arr) = [4,5,6,1,2,3];

Write in your own words what "unshift" means?

It updates the array by adding the given values at the beginning of the array.


Does it mutate the original array?

Yes it mutates the original array.

## indexOf

What are the parameters?

It accepts two parameters one for search element and the other to specify the starting index to begin search.

What will it return?

It returns the first index of the search element in the given array if it is found otherwise it returns -1.

Give an Example.

let arr = [1,2,3];

console.log(arr.indexOf(3)); // 2

Write in your own words what "indexOf" means?

This method searches for the given element in the array and returns the index where it first encounters it.

Does it mutate the original array?

No it does not.

## lastIndexOf

What are the parameters?

It accepts two parameters one the search element and the second the starting index to begin search.

What will it return?

It will return the last index at which the given element could be found in the given array.

Give an Example.

let arr = [1,2,3,3];

console.log(arr.lastIndexOf(3)); // 3

Write in your own words what "lastIndexOf" means?

It just searches for the last index at which given element could be found in the array.

Does it mutate the original array?

No it does not mutate the original array.

## includes

What are the parameters?

It has two parameters one is the element to search for and the other is the index to begin our search with.

What will it return?

It returns either true or false.

Give an Example.

let arr =[1,2,3,4];

console.log(arr.includes(3)); // true

Write in your own words what "includes" means?

It searches the array for given value if the array contains that value it will return true;

Does it mutate the original array?

No it does not.

## reverse

What are the parameters?

It does not have any parameters.

What will it return?

It will reverses the entire array and returns it.

Give an Example.

let arr = [2,3,4];

console.log(arr.reverse()); // [4,3,2];

Write in your own words what "reverse" means?

It reverses the entire array in place.

Does it mutate the original array?

Yes it mutates the original array.

## splice

What are the parameters?

It has a start parameter and delete count parameter and the elements that we want to add at start index.

What will it return?

An array containing the deleted elements.

Give an Example.

let arr = [1,2,3,4,5];

console.log(arr.splice(2,2,['ammulu','nani])); // [3,4]

Write in your own words what "splice" means?

It can be used to add elements in middle of the array and it is also useful in deleting the elements from middle of the array. 

Does it mutate the original array?

Yes it mutates the original array.

## slice

What are the parameters?

It takes two parameters start and end.


What will it return?

It returns a new array which has all the elements extracted from the start to end.

Give an Example.

let arr = [2,3,4,5];

console.log(arr.slice(1,4)); // [3,4,5]

Write in your own words what "slice" means?

It slices the the given array at the start and at the end and returns that part as a new array.

Does it mutate the original array?

It does not mutate the original array.

## foreach

What are the parameters?

It has only one parameter that takes the callback function.

What will it return?

It returns undefined.

Give an Example.

let arr = [1,2,3];

function callback(x) {
    
    console.log(x);
}

console.log(arr.foreach(callback)); // 1 2 3

Write in your own words what "foreach" means?

It iterates through the entire array passing each element to the function.

Does it mutate the original array?

No it does not mutate the original array.

## map

What are the parameters?

It takes one essential parameter which is callback function and an optional parameter where we can pass 'this'.

What will it return?

A new array where each element is obtained by passing the elements of the original array to the function.

Give an Example.

let arr =[1,2,3];

console.log(arr.map((element)=> {2*element}))

Write in your own words what "map" means?

It iterates through the array passes the elements to the callback function and puts the return value into a new array.

Does it mutate the original array?

No it does not mutate.

## filter

What are the parameters?

It accepts a callback function as parameter.

What will it return?

A new array consisting of all the elements in the original array which returns true when supplied to our callback function.

Give an Example.

let arr = [1,2,3];

console.log(arr.filter((elem)=> { return elem > 2}); // [3]

Write in your own words what "filter" means?

This method filters out the elements from the parent array which do not result in true when supplied to the callback function and only returns the other elements in a new array.

Does it mutate the original array?

No it does not mutate the original array.

## find

What are the parameters?

It accepts a callback function as a parameter.

What will it return?

The first element in the array which results in true when passed to the callback function. If no element returns true it return undefined.

Give an Example.

let arr = [1,2,3];

console.log(arr.find((elem)=>((elem%2) == 1))); // 1

Write in your own words what "find" means?

It finds the first element that returns true when passed to callback function and it returns that element.

Does it mutate the original array?

No it does not.

## findIndex

What are the parameters?

It accepts a callback function as a parameter.

What will it return?

It returns the index of the first element in the array which returns true when passed to the callback function. If no element returns true it returns -1.

Give an Example.

let arr = [1,2,4,8];

console.log(arr.findIndex((elem)=> {return (elem%2) == 0})); // 1 the index of 2

Write in your own words what "findIndex" means?

It returns the index of the first element which satisfies our condition, if no element passes our condition it will return -1.

Does it mutate the original array?

No it does not mutate the original array.

## some

What are the parameters?

It accepts the callback function as its first parameter and this as an optional parameter.

What will it return?

It returns true if atleast one element passes the given condition otherwise it return false. A boolean value.

Give an Example.

let arr = [1,2,3];

console.log(arr.some((elem)=>(elem%2 == 0))); // true

Write in your own words what "some" means?

A method which return true even if one element passes the test if nothing passes then it would return false.

Does it mutate the original array?

No it does not mutate the original array.

## every

What are the parameters?

It needs a callback function as the main parameter and 'this' as an optional parameter.

What will it return?

It returns true if every element returns true when passed to the callback function otherwise false.

Give an Example.

let arr = [1,2,3];

console.log(arr.every((elem)=>(elem>0))); // true

Write in your own words what "every" means?

If every element in the array satisfies the condition we provided then this method returns true else it returns false.

Does it mutate the original array?

No it does not mutate the original array.


## sort

What are the parameters?

It takes an optional comparision function as a parameter.

What will it return?

It retuns the original array sorted in place.

Give an Example.

let arr = [3,2,0];

console.log(arr.sort((a,b)=> a-b)); // [0,2,3]

Write in your own words what "sort" means?

The default compare function is based upon the UTF -16 code and sorts them in an ascending order of that code. If we have numbers then we've to write compare function.

Does it mutate the original array?

Yes it does mutate the original array.

## reduce

What are the parameters?
 
It takes two parameters one is callback function and the other is accumulator value.

What will it return?

A value that results from reducer callback function.

Give an Example.

let a = [1,2,3,4];

console.log(a.reduce((sum,elem) => sum+element,0)); // 10


Write in your own words what "reduce" means?

It reduces the entire array to a single value.

Does it mutate the original array?

No it does not mutate the original array.


